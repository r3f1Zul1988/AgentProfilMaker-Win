﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Drawing;
using System.Diagnostics;

namespace AgentProfilMaker
{
	//ToDO
	//	*Save cut Pics
	//	*Find overlaps

	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
		public MainWindow()
		{
			InitializeComponent();
		}

		private void btnSavePicture_Click(object sender, RoutedEventArgs e)
		{


			SaveFileDialog saveFileDialog1 = new SaveFileDialog();
			saveFileDialog1.Filter = "JPeg Image|*.jpg|Bitmap Image|*.bmp|Gif Image|*.gif";
			saveFileDialog1.Title = "Save an Image File";
			saveFileDialog1.InitialDirectory = txtDirectory.Text;

			List<BitmapImage> imgList = new List<BitmapImage>();
			var height = 0.0;
			var width = new BitmapImage(new Uri(lstPictures.Items[0].ToString())).Width;
			for (int i = 0; i < lstPictures.Items.Count; i++)
			{
				var img = new BitmapImage(new Uri(lstPictures.Items[i].ToString()));
				imgList.Add(img);
				height += img.Height;
			}

			saveFileDialog1.FileName = DateTime.Now.ToShortDateString() + "_" + txtAgentname.Text + "_" + Convert.ToInt32(width).ToString() + "x@";
			if (saveFileDialog1.ShowDialog() == true)
			{
				
				BitmapFrame frame1 = BitmapDecoder.Create(new Uri(imgList[0].ToString()), BitmapCreateOptions.None, BitmapCacheOption.OnLoad).Frames.First();

				var imageHeight = 0;

				var drawingVisual = new DrawingVisual();
				using (DrawingContext drawingContext = drawingVisual.RenderOpen())
				{
					var x = 0;
					for (int i = 0; i < imgList.Count; i++)
					{
						var pos = 0;
						if (i < imgList.Count - 1)
						{
							drawingContext.DrawImage(imgList[i], new Rect(0, x, imgList[i].Width, imgList[i].Height));
							pos = FindOverlapPosition(imgList[i], imgList[i + 1]);
							x += pos;
							if (imageHeight == 0)
								imageHeight = (int)imgList[i].Height + pos;
							else
								imageHeight+= pos;
						}
						else
						{
							drawingContext.DrawImage(imgList[i], new Rect(0, x, imgList[i].Width, imgList[i].Height));
						}
					}
				}
				var bmp = new RenderTargetBitmap(Convert.ToInt32(frame1.Width), imageHeight, 96, 96, PixelFormats.Pbgra32);
				bmp.Render(drawingVisual);

				var encoder = new PngBitmapEncoder();
				encoder.Frames.Add(BitmapFrame.Create(bmp));

				var filename = saveFileDialog1.FileName.Replace("@", imageHeight.ToString());
				using (Stream stream = File.Create(filename))
					encoder.Save(stream);
				MessageBox.Show("Bild ist fertig");
			}
		}

		private void btnSelectImages_Click(object sender, RoutedEventArgs e)
		{
			OpenFileDialog openFileDialog = new OpenFileDialog();

			openFileDialog.Multiselect = true;
			openFileDialog.Title = "Select pictures";
			openFileDialog.Filter = "All supported graphics|*.jpg;*.jpeg;*.png|" + "JPEG (*.jpg;*.jpeg)|*.jpg;*.jpeg|" + "Portable Network Graphic (*.png)|*.png";
			openFileDialog.InitialDirectory = txtDirectory.Text;

			if (openFileDialog.ShowDialog() == true)
			{
				foreach (var file in openFileDialog.FileNames)
					lstPictures.Items.Add(file.ToString());
			}
		}

		private void lstPictures_SelectionChanged(object sender, SelectionChangedEventArgs e)
		{
			if (lstPictures != null)
				ImgPreview.Source = new BitmapImage(new Uri(lstPictures.SelectedItem.ToString()));
		}

		private void nextPic_Click(object sender, RoutedEventArgs e)
		{
			if (lstPictures.SelectedIndex == lstPictures.Items.Count - 1)
				lstPictures.SelectedIndex = 0;
			else
				lstPictures.SelectedIndex += 1;
		}

		private void prevPic_Click(object sender, RoutedEventArgs e)
		{
			if (lstPictures.SelectedIndex == 0)
				lstPictures.SelectedIndex = lstPictures.Items.Count - 1;
			else
				lstPictures.SelectedIndex -= 1;
		}

		private void cutPic_Click(object sender, RoutedEventArgs e)
		{
			int cropSize = Convert.ToInt32(txtCropSize.Text);

			List<string> imgLst = new List<string>();
			for (int i = 0; i < lstPictures.Items.Count; i++)
			{
				imgLst.Add((string)lstPictures.Items[i]);
				var dec = BitmapDecoder.Create(new Uri(imgLst[i]), BitmapCreateOptions.None, BitmapCacheOption.None);
				var src = dec.Frames[0];

				var crop = new CroppedBitmap(src, new Int32Rect(0, cropSize, src.PixelWidth, src.PixelHeight - cropSize));

				var drawingVisual = new DrawingVisual();

				using (DrawingContext drawingContext = drawingVisual.RenderOpen())
				{
					drawingContext.DrawImage(crop, new Rect(0, 0, crop.Width, crop.Height));
				}
				var bmp = new RenderTargetBitmap(Convert.ToInt32(crop.Width), Convert.ToInt32(crop.Height), 96, 96, PixelFormats.Pbgra32);
				bmp.Render(drawingVisual);

				var encoder = new PngBitmapEncoder();
				encoder.Frames.Add(BitmapFrame.Create(bmp));

				var filename = txtDirectory.Text + "/" + i.ToString() + "_new.png";

				lstPictures.Items.RemoveAt(i);

				lstPictures.Items.Insert(i, filename);

				using (Stream stream = File.Create(filename))
				{
					encoder.Save(stream);
					MessageBox.Show("Bild ist fertig");
				}
			}
		}


		private void btnDirectory_Click(object sender, RoutedEventArgs e)
		{
			System.Windows.Forms.FolderBrowserDialog fbd = new System.Windows.Forms.FolderBrowserDialog();

			System.Windows.Forms.DialogResult result = fbd.ShowDialog();

			if (!string.IsNullOrWhiteSpace(fbd.SelectedPath))
			{
				txtDirectory.Text = fbd.SelectedPath;
			}
		}

		private void RemoveBackground(byte[] image, byte minBrightness)
		{
			for (int i = 0; i < image.Length; i += 4)
			{
				var avg = (byte)((image[i] + image[i + 1] + image[i + 2]) / 3);
				if (avg < minBrightness)
					image[i] = image[i + 1] = image[i + 2] = 0;
			}
		}

		/// <summary>
		/// Returns the offset of image two in image one
		/// </summary>
		/// <param name="one">Image one</param>
		/// <param name="two">Image two</param>
		/// <returns>The offset or -1 of no overlap was found</returns>
		private int GetOffset(byte[] one, byte[] two, int stride)
		{
			var smallestDiff = float.MaxValue;
			var offset = -1;

			for (int i = 1; i < (one.Length / stride) * 0.85; i++)
			{
				var imgDiff = GetImageDifference(one, two, i * stride);
				if (imgDiff < smallestDiff)
				{
					offset = i;
					smallestDiff = imgDiff;
				}
			}
			if (offset == -1)
				throw new IOException("overlap not found");
			return offset;
		}

		private float GetImageDifference(byte[] one, byte[] two, int offset)
		{
			float rgb = 0;

			for (int o = 0; o < one.Length - offset; o += 4)
			{
				rgb += Math.Abs(one[o + offset] - two[o])
				 + Math.Abs(one[o + offset + 1] - two[o + 1])
				 + Math.Abs(one[o + offset + 2] - two[o + 2]);
			}
			rgb /= (one.Length - offset) / 4;

			return rgb / 3;
		}

		private byte[] BitmapImageToByteArray(BitmapImage img)
		{
			//Debug.Assert(img.Format.BitsPerPixel == 24);

			var stride = img.PixelWidth * 4;
			var res = new byte[img.PixelHeight * stride];
			img.CopyPixels(res, stride, 0);
			return res;
		}

		private void SaveAsImage(string path, byte[] pixels, int width, int height)
		{
			using (Stream stream = File.Create(path))
			{
				var encoder = new PngBitmapEncoder();
				encoder.Frames.Add(BitmapFrame.Create(BitmapSource.Create(width, height, 96, 96, PixelFormats.Pbgra32, null, pixels, width * 4)));
				encoder.Save(stream);
			}
		}

		private int FindOverlapPosition(BitmapImage img0, BitmapImage img1)
		{

			Debug.Assert
			(
				img0.PixelWidth == img1.PixelWidth &&
				img0.PixelHeight == img1.PixelHeight
			);

			var pixels0 = BitmapImageToByteArray(img0);
			var pixels1 = BitmapImageToByteArray(img1);

			RemoveBackground(pixels0, 75);
			RemoveBackground(pixels1, 75);

			//SaveAsImage("C:\\Users\\Marcel\\Pictures\\r3f1zul\\01-gray.png", pixels0, img0.PixelWidth, img0.PixelHeight);
			//SaveAsImage("C:\\Users\\Marcel\\Pictures\\r3f1zul\\02-gray.png", pixels1, img1.PixelWidth, img0.PixelHeight);

			return GetOffset(pixels0, pixels1, img0.PixelWidth * 4);
		}
	}
}
